import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, catchError, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  
  private baseUrl = "https://kautilyasikar.institute.org.in/api";

   
   

  constructor(private http:HttpClient,private router:Router) { }
  
  
   signinUser(data:any):Observable<any>{
    return this.http.post<any>(this.baseUrl+'/auth/signin',data);
   }
   
  
}

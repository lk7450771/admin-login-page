import { DOCUMENT } from '@angular/common';
import { HttpInterceptorFn} from '@angular/common/http';
import { inject } from '@angular/core';

export const loggerInterceptor: HttpInterceptorFn = (req, next) => {
  const localStorage=inject(DOCUMENT);
    const Token=localStorage.defaultView?.localStorage.getItem('authToken')
    if(Token){
      req =req.clone({
        setHeaders:{
          token:Token
        }
      });
    }
  return next(req);
};


import { DOCUMENT } from '@angular/common';
import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';

export const authGuard: CanActivateFn = (route, state) => {
  const router=inject(Router);
  const localStorage=inject(DOCUMENT);
 const token= localStorage.defaultView?.localStorage.getItem('authToken');
 if(token){
  return true;
 }else{
  alert('login first')
  router.navigate(['login'])
  return false;
 } 
};

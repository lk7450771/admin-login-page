import { CommonModule, DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { RouterLink } from '@angular/router';
import { AuthService } from '../../auth.service';

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [RouterLink,ReactiveFormsModule,CommonModule],
  templateUrl: './home.component.html',
  styleUrl: './home.component.css'
})
export class HomeComponent {
 
  user:any;
  client:any;
  token:any;
  constructor(@Inject(DOCUMENT) private document: Document,){}
   
  getclient(){
    let client;
    const localStorage=this.document.defaultView?.localStorage;
    if(localStorage){
      client=localStorage.getItem('client');
    }
    if(client){
      this.client=JSON.parse(client);
    }else{
      client=null;
    }
    console.log(client);
  }
  // This is get user data 
  getuser(){
    let user;
    const localStorage=this.document.defaultView?.localStorage;
    // ye Condition data (user data) ko local storege se lene k liye
    if(localStorage){
      user=localStorage.getItem('user');
    }
    if(user){
      this.user=JSON.parse(user);
    }else{
      this.user=null;
    }
    console.log(this.user);
  }
  

}




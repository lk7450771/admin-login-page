import { CommonModule, DOCUMENT } from '@angular/common';
import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';
import { Router, RouterLink } from '@angular/router';
import { AuthService } from '../../auth.service';
import { error } from 'console';



@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule,RouterLink,CommonModule,],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  // This Variable 
loginform: FormGroup<any>;
// These Two variables for form validaction
islogin:boolean=false;
invalid:boolean=false;

  // document: any;

  constructor(private fb:FormBuilder,private auth:AuthService,private router:Router){
    this.loginform=this.fb.group({
      username:['',[Validators.required,Validators.minLength(5),Validators.maxLength(20)]],
      password:['',[Validators.required],]
    })
  }
 



  login(){
    // This is condation for form validation. 
    let user=this.loginform.value;
    if(!this.loginform.valid){
      this.islogin=false
      this.invalid=true;
      return
    }
    else if(this.loginform.valid){
      this.islogin=false
      this.invalid=false;
      this.loginform.reset();
    }
    // console.log(user);
    // This condition for api se data ko local storege m save karne ki hai 
    
    this.auth.signinUser(user).subscribe({next:(res:any)=>{
      if(res && res.token){
        localStorage.setItem('authToken',res.token);
        localStorage.setItem('user',JSON.stringify(res.user));
        localStorage.setItem('client',JSON.stringify(res.client))
        this.router.navigate([''])
      }
      else{
        window.alert('Invalid username or password')
      }
    },
    error:(err:any)=>{
      console.log('wrong pssword',err);
        	window.alert(err.error?err.error.message:err)
          
    }
  })
  }

}


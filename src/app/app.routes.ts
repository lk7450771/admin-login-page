import { Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { AdminComponent } from './components/admin/admin.component';
import { LoginComponent } from './components/login/login.component';

import { StudentComponent } from './components/student/student.component';
import { authGuard } from './auth.guard';

export const routes: Routes = [
    {path:'', component:HomeComponent},
    {path:'admin', component:AdminComponent,canActivate:[authGuard]},
    {path:'student', component:StudentComponent,canActivate:[authGuard]},
    {path:'login', component:LoginComponent,}
];
